# pollyone

polls for everything!

## Contents:

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)

## About The Project

Our Project consist of an user-friendly mobile app that will help you to make public polls worldwide directly from your smartphone.

Here's why:
* Your time should be focused on creating something amazing. Others app are slow and private, we are going to develop the most user-friendly app for every polls you need 
* you' ll never have to check websites again, because you have a light and dedicated app to make your polls
* You can fork and customize this app as you prefer! :smile:

Nice, now you can download, test, modify, and customize your personal polls app!

### Built With
Here you can find the technologies we used to build our app.
* [Django-Rest-Framework](https://www.django-rest-framework.org/)
* [Flutter](https://flutter.dev/)
* [Docker](https://www.docker.com/)
